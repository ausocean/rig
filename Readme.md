# Readme

Repository for AusOcean CAD files, PCB design files, 3D models and 3D modelling scripts.

Create a directory for new components and use lowercase with underscores for directory and file names. Acronyms can be capitalised. Eg. DO\_salinity\_sensor\_holder.stl 

# License

This software is Copyright (C) 2018-2023 the Australian Ocean Lab (AusOcean).

It is free software: you can redistribute it and/or modify them
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

It is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with NetReceiver in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
